FROM node:10.16.0-stretch

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

RUN npm i

# Bundle app source
COPY . .

ARG PORT=3000
ARG NODE_ENV=production

ENV NODE_ENV $NODE_ENV
ENV PORT $PORT

# build and prune devdependencies
RUN npm run build && npm run prune

# default to port 3000 for node, and 9229 for debug
EXPOSE $PORT 9229

CMD npm run start