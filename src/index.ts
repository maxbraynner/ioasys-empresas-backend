import { envVars } from "./env";

import database from "./storage/postgres";
import app from "./app";

(async () => {
    await database.isReady();

    app.listen(envVars.PORT, () => {
        console.log("Listening on port: " + envVars.PORT);
    });
})().catch(error => console.log(error));
