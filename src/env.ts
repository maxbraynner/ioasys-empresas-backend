import * as Joi from "joi";
import * as dotenv from "dotenv";

interface EnvVars {
    NODE_ENV: string;
    PORT: string;
    isDev: () => boolean;
    isProd: () => boolean;
    isTest: () => boolean;

    JWT_SECRET: string;
    DATABASE_URL: string;
}

const envSchema = {
    // app
    NODE_ENV: Joi.string()
        .valid("development", "test", "production")
        .allow(""),
    PORT: Joi.number().allow(""),
    JWT_SECRET: Joi.string().required(),

    // Postgres
    DATABASE_URL: Joi.string().when("NODE_ENV", {
        is: "test",
        then: Joi.allow(""),
        otherwise: Joi.required()
    })
};

function parse(): EnvVars {
    const { NODE_ENV = "development" } = process.env;
    if (NODE_ENV === "development" || NODE_ENV === "test") {
        dotenv.config();
    }

    const { error, value: envVars } = Joi.validate(process.env, envSchema, {
        stripUnknown: true
    });

    if (error) {
        throw new Error(error.message);
    }

    return {
        NODE_ENV: envVars.NODE_ENV || NODE_ENV,
        DATABASE_URL: envVars.DATABASE_URL,
        PORT: envVars.PORT || 3000,

        JWT_SECRET: envVars.JWT_SECRET,

        isDev: () => envVars.NODE_ENV === "development",
        isProd: () => envVars.NODE_ENV === "production",
        isTest: () => envVars.NODE_ENV === "test"
    } as EnvVars;
}

const envVars = parse();

export { envVars };
