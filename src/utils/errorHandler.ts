import * as Boom from "boom";
import { Request, Response, NextFunction } from "express";
import {
    ValidationError as SequelizeValidationError,
    UniqueConstraintError,
    EmptyResultError,
    DatabaseError,
    ForeignKeyConstraintError
} from "sequelize";
import { envVars } from "../env";

/**
 * wrap a controller call with error handler
 * @param controllerCall controller method to call
 */
export const wraper = (controllerCall: Function) => (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    controllerCall(req, res).catch(next);
};

/**
 * catch boom error
 * @param error
 * @param req
 * @param res
 * @param next
 */
export function boomHanlder(
    error,
    req: Request,
    res: Response,
    next: NextFunction
) {
    if (!error.isBoom) {
        next(error);
        return;
    }

    const {
        statusCode,
        payload: { message }
    } = error.output;

    if (envVars.isDev()) {
        console.error(error);
    }

    res.status(statusCode).json({
        success: false,
        errors: [message]
    });
}

/**
 * catch internal error
 * @param error
 * @param req
 * @param res
 * @param next
 */
export function internalHandler(
    error,
    req: Request,
    res: Response,
    next: NextFunction
) {
    const outputError = {
        success: false,
        errors: ["An unexpected error occurred"]
    };

    // internal error
    if (error.status >= 500 || !error.status) {
        console.error(error);
        if (envVars.isDev()) {
            outputError.errors = [error.message];
        }
    } else {
        next(error);
        return;
    }

    res.status(error.status || 500).json(outputError);
}

/**
 * catch sequelize's error
 * @param error
 * @param req
 * @param res
 * @param next
 */
export function sequelizeHandler(
    error,
    req: Request,
    res: Response,
    next: NextFunction
) {
    switch (error.constructor) {
        case SequelizeValidationError:
            throw Boom.badRequest(error);
        case UniqueConstraintError:
            throw Boom.conflict(error);
        case EmptyResultError:
            throw Boom.notFound(error);
        case DatabaseError:
            throw Boom.badRequest(error);
        case ForeignKeyConstraintError:
            throw Boom.badData(
                "Update or Delete violates a foreign key constraint"
            );
        default:
            next(error);
    }
}
