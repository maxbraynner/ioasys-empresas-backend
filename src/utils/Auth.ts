import * as Boom from "boom";
import { NextFunction, Response, Request } from "express";
import jwt from "../utils/JwtUtil";

class Auth {
    /**
     * Express middleware that validates Tokens passed in HTTP header.
     * @param req
     * @param res
     * @param next
     */
    private async verify(req: Request, res: Response, next: NextFunction) {
        try {
            const accessToken = (req.headers["access-token"] as string) || "-";
            const client = (req.headers["client"] as string) || "-";
            const uid = (req.headers["uid"] as string) || "-";

            if (!accessToken) {
                next(Boom.unauthorized("Authorized users only."));
                return;
            }

            const token = this.getToken(accessToken);
            const decoded = await jwt.verify(token, client, uid);

            res.header("token-type", "Bearer");
            res.header("access-token", token);
            res.header("client", client);
            res.header("uid", uid);
            res.header("expiry", `${decoded.exp}`);

            next();
        } catch (error) {
            next(
                error.isBoom
                    ? error
                    : Boom.unauthorized("Authorized users only.")
            );
        }
    }

    get config() {
        return this.verify.bind(this);
    }

    private getToken(authorization: string): string {
        const bearer = authorization.split("Bearer ");
        return (bearer[1] || bearer[0]).trim();
    }
}

export default new Auth().config;
