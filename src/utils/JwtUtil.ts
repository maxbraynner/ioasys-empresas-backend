import * as jwt from "jsonwebtoken";
import { envVars } from "../env";

class JwtUtils {
    sign(payload: any, issuer: string, subject: string): Promise<string> {
        return new Promise((resolve, reject) => {
            jwt.sign(
                payload,
                envVars.JWT_SECRET,
                { expiresIn: "1 hours", issuer, subject },
                (err, token) => {
                    if (err) {
                        reject(err);
                        return;
                    }
                    resolve(token);
                }
            );
        });
    }

    verify(
        token: string,
        issuer: string,
        subject: string
    ): Promise<{ exp?: number; iss?: string; sub?: string }> {
        return new Promise((resolve, reject) => {
            jwt.verify(
                token,
                envVars.JWT_SECRET,
                { issuer, subject },
                (err, decoded: {}) => {
                    if (err) {
                        reject(err);
                        return;
                    }
                    resolve(decoded);
                }
            );
        });
    }
}

export default new JwtUtils();
