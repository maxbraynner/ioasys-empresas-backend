import { Sequelize } from "sequelize";
import {
    Investor,
    EnterpriseType,
    Enterprise,
    InvestorPortfolio
} from "./models";
import { envVars } from "../../env";
const Config = require("./config");

class DataBase {
    private _connection: Sequelize;

    public get connection(): Sequelize {
        return this._connection;
    }

    public async isReady() {
        await this._connection.authenticate();
    }

    private connect() {
        if (envVars.isTest()) {
            // sqlite database
            this._connection = new Sequelize("database", null, null, Config);
        } else {
            // postgres database
            this._connection = new Sequelize(Config.url, Config);
        }
    }

    /**
     * models initiations
     */
    private defineModels() {
        Investor.define(this._connection);
        EnterpriseType.define(this._connection);
        Enterprise.define(this._connection);
        InvestorPortfolio.define(this._connection);
    }

    /**
     * make all the associations shine
     */
    private associateModels() {
        Investor.associate();
        Enterprise.associate();
    }

    public configure() {
        this.connect();
        this.defineModels();
        this.associateModels();
    }
}

export default new DataBase();
