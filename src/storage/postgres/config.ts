import { envVars } from "../../env";

const env = envVars.NODE_ENV;

const defaultConfig = {
    logging: false,
    dialect: "postgres",
    seederStorage: "sequelize",
    seederStorageTableName: "SequelizeSeedMeta",
    url: envVars.DATABASE_URL,
    define: {
        freezeTableName: true
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 60000,
        idle: 10000
    }
};

const sequelizeConfig = {
    development: {
        ...defaultConfig,
        benchmark: true
        // logging: true,
    },
    test: {
        ...defaultConfig,
        dialect: "sqlite",
        storage: "./__tests__/database.sqlite",
        url: null
    },
    production: {
        ...defaultConfig,
        ssl: true,
        dialectOptions: {
            ssl: {
                require: true
            }
        }
    }
};

module.exports = sequelizeConfig[env];