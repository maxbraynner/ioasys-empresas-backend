import { Model, DataTypes, Sequelize } from "sequelize";

export class EnterpriseType extends Model {
    id: string;
    enterprise_type_name: string;

    public static define(sequelize: Sequelize) {
        this.init(
            {
                id: {
                    type: DataTypes.INTEGER,
                    primaryKey: true,
                    allowNull: false,
                    autoIncrement: true
                },
                enterprise_type_name: {
                    type: DataTypes.STRING,
                    allowNull: false
                }
            },
            {
                sequelize,
                tableName: "tb_enterprise_type",
                timestamps: false
            }
        );
    }
}
