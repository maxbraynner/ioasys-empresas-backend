import { Model, DataTypes, Sequelize } from "sequelize";
import { Enterprise } from "./enterprise.model";
import { InvestorPortfolio } from "./investor_portfolio.model";

export class Investor extends Model {
    id: string;
    investor_name: string;
    email: string;
    password: string;
    city: string;
    country: string;
    balance: number;
    photo: string;
    portfolio_value: number;
    first_access: boolean;
    super_angel: boolean;
    enterprise_id: number;

    portfolio?: InvestorPortfolio[];
    enterprise?: Enterprise;

    public static define(sequelize: Sequelize) {
        this.init(
            {
                id: {
                    type: DataTypes.INTEGER,
                    primaryKey: true,
                    allowNull: false,
                    autoIncrement: true
                },
                investor_name: {
                    type: DataTypes.STRING,
                    allowNull: false
                },
                email: {
                    type: DataTypes.STRING,
                    allowNull: false,
                    unique: true
                },
                password: {
                    type: DataTypes.STRING,
                    allowNull: false
                },
                city: {
                    type: DataTypes.STRING,
                    allowNull: false
                },
                country: {
                    type: DataTypes.STRING,
                    allowNull: false
                },
                balance: {
                    type: DataTypes.INTEGER,
                    allowNull: true
                },
                photo: {
                    type: DataTypes.STRING,
                    allowNull: true
                },
                portfolio_value: {
                    type: DataTypes.INTEGER,
                    allowNull: true
                },
                first_access: {
                    type: DataTypes.BOOLEAN,
                    allowNull: false
                },
                super_angel: {
                    type: DataTypes.BOOLEAN,
                    allowNull: false
                },
                enterprise_id: {
                    type: DataTypes.INTEGER,
                    allowNull: true,
                    references: {
                        model: "tb_enterprise",
                        key: "id"
                    }
                }
            },
            {
                sequelize,
                tableName: "tb_investor",
                timestamps: false
            }
        );
    }

    public static associate() {
        Investor.belongsTo(Enterprise, {
            foreignKey: "enterprise_id",
            as: "enterprise"
        });

        Investor.hasMany(InvestorPortfolio, {
            foreignKey: "investor_id",
            as: "portfolio"
        });
    }
}
