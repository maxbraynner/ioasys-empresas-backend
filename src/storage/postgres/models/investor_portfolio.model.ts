import { Model, DataTypes, Sequelize } from "sequelize";
import { Investor } from "./inverstor.model";
import { Enterprise } from "./enterprise.model";

export class InvestorPortfolio extends Model {
    investor_id: number;
    enterprise_id: number;

    investor?: Investor;
    enterprise?: Enterprise;

    public static define(sequelize: Sequelize) {
        this.init(
            {
                investor_id: {
                    type: DataTypes.INTEGER,
                    primaryKey: true,
                    allowNull: false,
                    autoIncrement: false,
                    references: {
                        model: "tb_investor",
                        key: "id"
                    }
                },
                enterprise_id: {
                    type: DataTypes.INTEGER,
                    primaryKey: true,
                    allowNull: false,
                    autoIncrement: false,
                    references: {
                        model: "tb_enterprise",
                        key: "id"
                    }
                }
            },
            {
                sequelize,
                tableName: "tb_investor_portfolio",
                timestamps: false
            }
        );
    }
}
