import { Model, DataTypes, Sequelize } from "sequelize";
import { EnterpriseType } from "./enterprise_type.model";

export class Enterprise extends Model {
    id: string;
    email_enterprise: string;
    facebook: string;
    twitter: string;
    linkedin: string;
    phone: string;
    own_enterprise: boolean;
    enterprise_name: string;
    photo: string;
    description: string;
    city: string;
    country: string;
    value: number;
    share_price: number;
    enterprise_type_id: number;
    shares: number
    own_shares: number

    enterprise_type?: EnterpriseType;

    public static define(sequelize: Sequelize) {
        this.init(
            {
                id: {
                    type: DataTypes.INTEGER,
                    primaryKey: true,
                    allowNull: false,
                    autoIncrement: true
                },
                email_enterprise: {
                    type: DataTypes.STRING,
                    allowNull: true
                },
                facebook: {
                    type: DataTypes.STRING,
                    allowNull: true
                },
                twitter: {
                    type: DataTypes.STRING,
                    allowNull: true
                },
                linkedin: {
                    type: DataTypes.STRING,
                    allowNull: true
                },
                phone: {
                    type: DataTypes.STRING,
                    allowNull: true
                },
                own_enterprise: {
                    type: DataTypes.BOOLEAN,
                    allowNull: true
                },
                enterprise_name: {
                    type: DataTypes.STRING,
                    allowNull: true
                },
                photo: {
                    type: DataTypes.STRING,
                    allowNull: true
                },
                description: {
                    type: DataTypes.TEXT,
                    allowNull: false
                },
                city: {
                    type: DataTypes.STRING,
                    allowNull: false
                },
                country: {
                    type: DataTypes.STRING,
                    allowNull: false
                },
                value: {
                    type: DataTypes.INTEGER,
                    allowNull: false
                },
                share_price: {
                    type: DataTypes.INTEGER,
                    allowNull: false
                },
                shares: {
                    type: DataTypes.INTEGER,
                    allowNull: true
                },
                own_shares: {
                    type: DataTypes.INTEGER,
                    allowNull: true
                },
                enterprise_type_id: {
                    type: DataTypes.INTEGER,
                    allowNull: false,
                    references: {
                        model: "tb_enterprise_type",
                        key: "id"
                    }
                }
            },
            {
                sequelize,
                tableName: "tb_enterprise",
                timestamps: false
            }
        );
    }

    public static associate() {
        Enterprise.belongsTo(EnterpriseType, {
            foreignKey: "enterprise_type_id",
            as: "enterprise_type"
        });
    }
}
