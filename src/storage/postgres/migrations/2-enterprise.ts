import { DataTypes, QueryInterface } from "sequelize";

module.exports = {
    up: (queryInterface: QueryInterface, sequelize) => {
        return queryInterface.createTable("tb_enterprise", {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            email_enterprise: {
                type: DataTypes.STRING,
                allowNull: true
            },
            facebook: {
                type: DataTypes.STRING,
                allowNull: true
            },
            twitter: {
                type: DataTypes.STRING,
                allowNull: true
            },
            linkedin: {
                type: DataTypes.STRING,
                allowNull: true
            },
            phone: {
                type: DataTypes.STRING,
                allowNull: true
            },
            own_enterprise: {
                type: DataTypes.BOOLEAN,
                allowNull: true
            },
            enterprise_name: {
                type: DataTypes.STRING,
                allowNull: true
            },
            photo: {
                type: DataTypes.STRING,
                allowNull: true
            },
            description: {
                type: DataTypes.TEXT,
                allowNull: false
            },
            city: {
                type: DataTypes.STRING,
                allowNull: false
            },
            country: {
                type: DataTypes.STRING,
                allowNull: false
            },
            value: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            share_price: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            shares: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            own_shares: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            enterprise_type_id: {
                type: DataTypes.INTEGER,
                allowNull: false,
                references: {
                    model: "tb_enterprise_type",
                    key: "id"
                }
            }
        });
    },

    down: (queryInterface: QueryInterface, sequelize) => {
        return queryInterface.dropTable("tb_enterprise");
    }
};
