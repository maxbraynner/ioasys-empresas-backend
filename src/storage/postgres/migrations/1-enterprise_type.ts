import { DataTypes, QueryInterface } from "sequelize";

module.exports = {
    up: (queryInterface: QueryInterface, sequelize) => {
        return queryInterface.createTable("tb_enterprise_type", {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            enterprise_type_name: {
                type: DataTypes.STRING,
                allowNull: false
            }
        });
    },

    down: (queryInterface: QueryInterface, sequelize) => {
        return queryInterface.dropTable("tb_enterprise_type");
    }
};
