import { DataTypes, QueryInterface } from "sequelize";

module.exports = {
    up: (queryInterface: QueryInterface, sequelize) => {
        return queryInterface.createTable("tb_investor_portfolio", {
            investor_id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: false,
                references: {
                    model: "tb_investor",
                    key: "id"
                }
            },
            enterprise_id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: false,
                references: {
                    model: "tb_enterprise",
                    key: "id"
                }
            }
        });
    },

    down: (queryInterface: QueryInterface, sequelize) => {
        return queryInterface.dropTable("tb_investor_portfolio");
    }
};
