import { DataTypes, QueryInterface } from "sequelize";

module.exports = {
    up: (queryInterface: QueryInterface, sequelize) => {
        return queryInterface.createTable("tb_investor", {
            id: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true
            },
            investor_name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false,
                unique: true
            },
            password: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            city: {
                type: DataTypes.STRING,
                allowNull: false
            },
            country: {
                type: DataTypes.STRING,
                allowNull: false
            },
            balance: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            photo: {
                type: DataTypes.STRING,
                allowNull: true
            },
            portfolio_value: {
                type: DataTypes.INTEGER,
                allowNull: true
            },
            first_access: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            super_angel: {
                type: DataTypes.BOOLEAN,
                allowNull: false
            },
            enterprise_id: {
                type: DataTypes.INTEGER,
                allowNull: true,
                references: {
                    model: "tb_enterprise",
                    key: "id"
                }
            }
        });
    },

    down: (queryInterface: QueryInterface, sequelize) => {
        return queryInterface.dropTable("tb_investor");
    }
};
