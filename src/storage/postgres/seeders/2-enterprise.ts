import * as Sequelize from "sequelize";

const enterprises: Object[] = require("../seeders_data/enterprises.json");

module.exports = {
    up: async (queryInterface: Sequelize.QueryInterface) => {
        try {
            await queryInterface.bulkInsert("tb_enterprise", enterprises);
        } catch (error) {
            console.log(error);
            throw error;
        }
    },

    down: async (queryInterface: Sequelize.QueryInterface) => {
        await queryInterface.bulkDelete("tb_enterprise", {
            id: enterprises.map(e => e["id"])
        });
    }
};
