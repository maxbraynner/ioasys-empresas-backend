import * as Sequelize from "sequelize";

const investor = {
    id: 1,
    investor_name: "Teste Apple",
    email: "testeapple@ioasys.com.br",
    password: "$2b$10$Om72mKCXfM.AlIp1JZxlR.paBJYeimxi5b0ThufmqpUF26ybtS8/G",
    city: "BH",
    country: "Brasil",
    balance: 1000000,
    photo: null,
    portfolio_value: 1000000,
    first_access: true,
    super_angel: false,
    enterprise_id: null
};

module.exports = {
    up: async (queryInterface: Sequelize.QueryInterface) => {
        try {
            await queryInterface.bulkInsert("tb_investor", [investor]);
        } catch (error) {
            console.log(error);
            throw error;
        }
    },

    down: async (queryInterface: Sequelize.QueryInterface) => {
        await queryInterface.bulkDelete("tb_investor", {
            id: investor.id
        });
    }
};
