import * as Sequelize from "sequelize";

const types: Object[] = require("../seeders_data/enterprise-type.json");

module.exports = {
    up: async (queryInterface: Sequelize.QueryInterface) => {
        try {
            await queryInterface.bulkInsert("tb_enterprise_type", types);
        } catch (error) {
            console.log(error);
            throw error;
        }
    },

    down: async (queryInterface: Sequelize.QueryInterface) => {
        await queryInterface.bulkDelete("tb_enterprise_type", {
            id: types.map(e => e["id"])
        });
    }
};
