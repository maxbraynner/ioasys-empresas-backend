import { Router } from "express";

// import sub-routers
import investor from "./v1/investor";
import enterprise from "./v1/enterprise";

const router = Router();

// mount express paths, any addition middleware can be added as well.
// ex. router.use('/pathway', middleware_function, sub-router);

/**
 * Health check
 */
router.get("/api", (req, res) => {
    res.json({ health: "ok" });
});

router.use("/api/v1/users", investor);
router.use("/api/v1/enterprises", enterprise);

// router export
export default router;
