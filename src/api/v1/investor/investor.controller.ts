import { Request, Response } from "express";
import * as boom from "boom";
import * as bcrypt from "bcrypt";
import * as uuidv4 from "uuid/v4";

import {
    Investor,
    Enterprise,
    InvestorPortfolio
} from "../../../storage/postgres/models";
import jwtUtils from "../../../utils/JwtUtil";

const message = "Invalid login credentials. Please try again.";

export class UserController {
    signIn = async (req: Request, res: Response) => {
        const {
            body: { email, password }
        } = req;

        if (!email || !password) {
            throw boom.unauthorized(message);
        }

        // consulta investor
        const investor = await Investor.findOne({
            where: {
                email
            },
            include: [
                { model: Enterprise, as: "enterprise" },
                { model: InvestorPortfolio, as: "portfolio" }
            ]
        });

        if (!investor) {
            throw boom.unauthorized(message);
        }

        // compara password
        const hasAccess = await bcrypt.compare(password, investor.password);
        if (!hasAccess) {
            throw boom.unauthorized(message);
        }

        // monta o token
        const client = uuidv4();
        const uid = investor.email;
        const token = await jwtUtils.sign({}, client, uid);

        // atribui dados do header
        res.header("token-type", "Bearer");
        res.header("access-token", token);
        res.header("client", client);
        res.header("uid", uid);

        const { portfolio, enterprise } = investor;

        // monta o retorno
        delete investor["dataValues"].password;
        delete investor["dataValues"].portfolio;
        delete investor["dataValues"].enterprise_id;
        delete investor["dataValues"].enterprise;
        const response = {
            investor: {
                ...investor["dataValues"],
                portfolio: {
                    enterprises_number: portfolio.length,
                    enterprises: portfolio
                }
            },
            enterprise,
            success: true
        };

        res.json(response);
    };
}
