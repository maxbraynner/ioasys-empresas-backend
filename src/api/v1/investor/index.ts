import { Router } from "express";
import { UserController } from "./investor.controller";
import { wraper } from "../../../utils/errorHandler";

const router = Router();
const controller = new UserController();

router.post("/auth/sign_in", wraper(controller.signIn));

export default router;
