import { Request, Response } from "express";
import { Op } from "sequelize";
import { Enterprise, EnterpriseType } from "../../../storage/postgres/models";

export class EnterpriseController {
    findById = async (req: Request, res: Response) => {
        const { id } = req.params;

        const enterprise = await Enterprise.findByPk(id, {
            include: [{ model: EnterpriseType, as: "enterprise_type" }],
            rejectOnEmpty: true
        });

        delete enterprise["dataValues"].enterprise_type_id;
        res.json({ enterprise, success: true });
    };

    find = async (req: Request, res: Response) => {
        const { name, enterprise_types } = req.query;

        const where = {};

        if (name) {
            where["enterprise_name"] = {
                [Op.iLike]: `%${name}%`
            };
        }

        if (enterprise_types) {
            if (enterprise_types instanceof Array) {
                where["enterprise_type_id"] = {
                    [Op.in]: enterprise_types
                };
            } else {
                where["enterprise_type_id"] = enterprise_types;
            }
        }

        let enterprises = await Enterprise.findAll({
            where,
            include: [{ model: EnterpriseType, as: "enterprise_type" }],
            order: [["id", "ASC"]]
        });

        enterprises.map(e => {
            delete e["dataValues"].enterprise_type_id;
            return e;
        });

        res.json({ enterprises });
    };
}
