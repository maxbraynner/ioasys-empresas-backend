import { Router } from "express";
import { EnterpriseController } from "./enterprise.controller";
import Auth from "../../../utils/Auth";
import { wraper } from "../../../utils/errorHandler";

const router = Router();
const controller = new EnterpriseController();

router.get("/:id", Auth, wraper(controller.findById));
router.get("/", Auth, wraper(controller.find));

export default router;
