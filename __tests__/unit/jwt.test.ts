import jwt from "../../src/utils/JwtUtil";
import * as uuidv4 from "uuid/v4";

describe("JWT", () => {
    let token = "",
        issuer = "testeapple@ioasys.com.br",
        subject = uuidv4();

    it("Generate JWT", async () => {
        token = await jwt.sign({}, issuer, subject);

        expect(token).toBeTruthy();
    });

    it("Verify JWT", async () => {
        const decoded = await jwt.verify(token, issuer, subject);

        expect(decoded.iss).toBe(issuer);
    });

    it("Verify Falsification", async () => {
        let error;
        try {
            await jwt.verify("wrong-token", issuer, subject);
        } catch (err) {
            error = err;
        }

        expect(error).toBeTruthy();
    });

    it("Verify issuer falsification", async () => {
        let error;
        try {
            await jwt.verify(token, "wrong-issuer", subject);
        } catch (err) {
            error = err;
        }

        expect(error).toBeTruthy();
    });

    it("Verify subject falsification", async () => {
        let error;
        try {
            await jwt.verify(token, issuer, "wrong-subject");
        } catch (err) {
            error = err;
        }

        expect(error).toBeTruthy();
    });
});
