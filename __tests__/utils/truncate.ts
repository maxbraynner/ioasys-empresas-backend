import { Enterprise, Investor, InvestorPortfolio } from "../../src/storage/postgres/models";

export const truncate = () => {
    InvestorPortfolio.destroy({ truncate: true, force: true });
    Investor.destroy({ truncate: true, force: true });
    Enterprise.destroy({ truncate: true, force: true });
};
