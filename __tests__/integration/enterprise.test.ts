import * as request from "supertest";
import app from "../../src/app";

describe("Enterprises", () => {
    let accessToken = "",
        client = "",
        uid = "";

    beforeAll(async () => {
        const response = await request(app)
            .post("/api/v1/users/auth/sign_in")
            .send({
                email: "testeapple@ioasys.com.br",
                password: "12341234"
            });

        accessToken = response.header["access-token"];
        client = response.header["client"];
        uid = response.header["uid"];
    });

    it("GET All Enterprises", async () => {
        const response = await request(app)
            .get("/api/v1/enterprises")
            .set("access-token", `Bearer ${accessToken}`)
            .set("client", client)
            .set("uid", uid);

        expect(response.status).toBe(200);
    });

    it("GET Enterprises Filtered", async () => {
        const response = await request(app)
            .get("/api/v1/enterprises")
            .query({
                enterprise_types: 1
            })
            .set("access-token", `Bearer ${accessToken}`)
            .set("client", client)
            .set("uid", uid);

        expect(response.status).toBe(200);
    });

    it("GET Enterprises Filtered by many Types", async () => {
        const response = await request(app)
            .get("/api/v1/enterprises?enterprise_types=1&enterprise_types=2")
            .set("access-token", `Bearer ${accessToken}`)
            .set("client", client)
            .set("uid", uid);

        expect(response.status).toBe(200);
    });

    it("GET Enterprise by ID", async () => {
        const response = await request(app)
            .get("/api/v1/enterprises/1")
            .set("access-token", `Bearer ${accessToken}`)
            .set("client", client)
            .set("uid", uid);

        expect(response.status).toBe(200);
    });

    it("GET Not Found", async () => {
        const response = await request(app)
            .get("/api/v1/enterprises/1000")
            .set("access-token", `Bearer ${accessToken}`)
            .set("client", client)
            .set("uid", uid);

        expect(response.status).toBe(404);
    });

    it("GET Unauthorized", async () => {
        const response = await request(app).get("/api/v1/enterprises");

        expect(response.status).toBe(401);
    });

    it("GET Unauthorized Client", async () => {
        const response = await request(app)
            .get("/api/v1/enterprises")
            .set("access-token", `Bearer ${accessToken}`)
            .set("client", "wrong")
            .set("uid", uid);

        expect(response.status).toBe(401);
    });

    it("GET Unauthorized Uid", async () => {
        const response = await request(app)
            .get("/api/v1/enterprises")
            .set("access-token", `Bearer ${accessToken}`)
            .set("client", client)
            .set("uid", "wrong");

        expect(response.status).toBe(401);
    });
});
