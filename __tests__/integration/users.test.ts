import * as request from "supertest";
import app from "../../src/app";

describe("Users", () => {
    beforeAll(async () => {});

    it("POST Sign In", async () => {
        const response = await request(app)
            .post("/api/v1/users/auth/sign_in")
            .send({
                email: "testeapple@ioasys.com.br",
                password: "12341234"
            });

        expect(response.status).toBe(200);
    });

    it("POST Sign In Unauthorized", async () => {
        const response = await request(app)
            .post("/api/v1/users/auth/sign_in")
            .send({
                email: "testeapple@ioasys.com.br",
                password: "abc"
            });

        expect(response.status).toBe(401);
    });
});
